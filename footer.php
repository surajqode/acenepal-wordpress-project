<?php 

	//options
	$footerlogo_enable = get_field('footer_logo_enable','option'); 
	$footerlogo = get_field('footer_logo','option');
	$footercopyright =  get_field('footer_copyright','option');
	$footerContactDetail =  get_field('footer_contact_detail','option');
	$footerOperatingHours =  get_field('footer_operating_hours','option');
	$footerSocialIcons =  get_field('footer_social_icons','option');


	$footerclass = "";
	if($footerlogo && $footerlogo_enable) {	
		$footerclass .= " footer-logo-active";
	}
	$copyrightPre = '<p class="copyright">Copyright &copy;&nbsp;<span id="current-year"></span>';
	if($footercopyright) {
		$copyright = $copyrightPre . '&nbsp;' . $footercopyright; 
	} else {
		$copyright = $copyrightPre . '&nbsp;' . get_bloginfo('name') . '</p>';
	}
	
?>
<div class="form-section" id="contactForm">
    <div class="wrapper">
        <div class="home-form">
            <h4 class="form-title">Contact Us</h4>
            <?php echo do_shortcode('[contact-form-7 id="45" title="Homepage Form"]'); ?>
        </div>
    </div>
</div>
<?php include(locate_template('modules/footer-cta.php')); ?>
		<footer class="<?php echo $footerclass; ?>" id="contact">
			<div class="wrapper">	
				<div class="main-footer-container">
					<div class="footer-logo-section">	
						<?php if($footerlogo_enable &&	$footerlogo) { ?>
							<a href="<?php echo homeUrl(); ?>" class="logo">
								<img src="<?php echo $footerlogo; ?>" >
							</a>
						<?php } ?>
						<div class="misc">
							<?php echo $copyright; ?>
						</div>
					</div>
					<div class="hr-line"></div>
					<?php if($footerContactDetail) : ?>
						<div class="footer-contact">
							<h4><?php echo esc_html($footerContactDetail['title']);?></h4>
							<div class="footer-sub-description"><?php echo ($footerContactDetail['description']);?></div>
						</div>
					<?php endif; ?>
					<div class="hr-line"></div>
					<?php if($footerOperatingHours) : ?>
						<div class="footer-operating-hour">
						<h4><?php echo esc_html($footerOperatingHours['title']);?></h4>
						<div class="footer-sub-description"><?php echo ($footerOperatingHours['description']);?></div>
						</div>
					<?php endif; ?>
					<div class="hr-line"></div>
					<?php if($footerSocialIcons) : ?>
						<div class="footer-social-icon">
						<h4><?php echo esc_html($footerSocialIcons['title']);?></h4>
						<?php include(locate_template('/modules/social-media.php')); ?>
						<div class="footer-sub-description"><?php echo ($footerSocialIcons['description']);?></div>
						</div>
					<?php endif; ?>

					<div class="misc hide-desktop">
						<?php echo $copyright; ?>
					</div>
				</div>
			 </div>
		</footer>	
	<?php

	wp_footer();  
	?>
</body>
</html>