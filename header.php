<?php 
$global_social = true;
if(get_field('login_protection')){
	gatewayRedirect(passgateway());
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <?php include(locate_template('/modules/meta.php')); ?>
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo tempURL('/favicons/apple-touch-icon.png'); ?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo tempURL('/favicons/favicon-32x32.png'); ?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo tempURL('/favicons/favicon-16x16.png'); ?>">
    <link rel="mask-icon" href="<?php echo tempURL('/favicons/safari-pinned-tab.svg'); ?>" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
	<script src="https://kit.fontawesome.com/65c2584546.js" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://use.typekit.net/jmq5itk.css">
                
    <?php 
    if(!is_user_logged_in()) { 
        the_field('global_header_scripts','option');    
    } 
    ?>
	<script>
		var tempURL = "<?php echo tempURL('/'); ?>";
	</script>		
    <!-- <style>
        :root{
            /* --display-font: <?php //the_field('display_font','option'); ?>;
            --primary-font: <?php //the_field('primary_font','option'); ?>;
            --body-font: <?php //the_field('body_font','option'); ?>; */
        }
    </style>     -->
    <?php do_action('wp_head'); ?>
</head>

<?php 
$logo = get_field('main_logo','option');
$bodyclass = '';

// if( get_field('header_style') && get_field('page_header_style')) {
// 	$headerClasses = getSelect(get_field('header_style'));
// } else {
// 	$headerClasses = getSelect(get_field('header_style','option'));
// }
// $headerClasses .= ' ' . getMultiple(get_field('header_bar_options','option'));


// if(get_field('dark_theme')) {
// 	$bodyclass .= "dark-theme ";
// 	$headerClasses .= "dark ";
// }

// $phone = get_field('telephone','option');

// if($phone) { 
// 	$phonehtml = '<a href="tel:' . preg_replace('/\D/',"",$phone) . '" class="telephone-fixed" >';
// 	$phonehtml .= '<p>' . $phone . '</p>';
// 	$phonehtml .= '</a>';
// } 

if(matchurl('lightbox-gallery') || get_field('hide_header')) {
	$hideheader = true;
} else{
	$hideheader = false;
}

//protection
$usealt_nav = get_field('protected_pages_alternative_menu','option');
if (($usealt_nav && get_field('login_protection')) || is_singular('testimonies') || is_singular('resources')){
	$nav_menu = 'protected_menu';
} else {
	$nav_menu = 'main_menu';
}

 
if(!is_front_page()) {
	$headerClasses = 'solid';
} else {
	$headerClasses = 'main_home_header';
}

?>

<body <?php body_class($bodyclass); ?>>

	<?php include(locate_template('/modules/annoucement.php')); ?>

	<?php if(!$hideheader) { ?>
		<header class="<?php echo $headerClasses; ?>">
			<div class="wrapper header-row <?php the_field('header_width','option'); echo ' '; the_field('header_layout','option'); ?>">			
					<?php include(locate_template('/modules/header-logo.php')); ?>
					<div class="header-content">
						<?php
						if (has_nav_menu($nav_menu)) {
							wp_nav_menu(
								array(
								'theme_location' => $nav_menu,
								'container'      => 'nav'
								)
							);
						}
						?>
						<?php 
						$headerImg = get_field('header_image','option');  
						$headerImgLink	= get_field('header_image_link','option');  
						if($headerImgLink) { ?>			
							<a href="<?php echo $headerImgLink; ?>" target="_blank" class="header-img" >
								<img src="<?php //echo $headerImg['url']; ?>"/>	
							</a>
						<?php } elseif($headerImg) { ?>
							<img class="header-img" src="<?php echo $headerImg['url']; ?>" alt="<?php echo $headerImg['alt']; ?>" />	
						<?php } 
						if($tel = get_field('telephone','option')) {
							echo '<a href="tel:' . $tel . ' " class="phone">';
							echo '<p>' . $tel . '</p>';
							echo '</a>';			
						}
						if (function_exists( 'is_woocommerce_activated')) {
							echo do_shortcode('[woocommerce_cart_icon]');
						}
						?>
						<?php include(locate_template('/modules/social-media-links.php')); ?>
						<?php if (has_nav_menu($nav_menu)) { ?>
						<div class="menu-toggle">
							<div class="one"></div>
							<div class="two"></div>
							<div class="three"></div>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</header>
	<?php } ?>