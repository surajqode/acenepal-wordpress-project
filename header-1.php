<?php 

get_header(); 

$logo = get_field('main_logo','option');

$global_social = true;

if( get_field('header_style') && get_field('page_header_style')) {
	$headerClasses = getSelect(get_field('header_style'));
} else {
	$headerClasses = getSelect(get_field('header_style','option'));
}
$headerClasses .= ' ' . getMultiple(get_field('header_bar_options','option'));

$bodyclass = '';
if(get_field('dark_theme')) {
	$bodyclass .= "dark-theme ";
	$headerClasses .= "dark ";
}

$phone = get_field('telephone','option');

if($phone) { 
	$phonehtml = '<a href="tel:' . preg_replace('/\D/',"",$phone) . '" class="telephone-fixed" >';
	$phonehtml .= '<p>' . $phone . '</p>';
	$phonehtml .= '</a>';
} 

if(matchurl('lightbox-gallery') || get_field('hide_header')) {
	$hideheader = true;
} else{
	$hideheader = false;
}

//protection
$usealt_nav = get_field('protected_pages_alternative_menu','option');
if (($usealt_nav && get_field('login_protection')) || is_singular('testimonies') || is_singular('resources')){
	$nav_menu = 'protected_menu';
} else {
	$nav_menu = 'main_menu';
}

?>

<body <?php body_class($bodyclass); ?>>

	<?php include(locate_template('/modules/annoucement.php')); ?>

	<?php if(!$hideheader) { ?>
		<header class="<?php echo $headerClasses; ?>">
			<div class="wrapper header-row <?php the_field('header_width','option'); echo ' '; the_field('header_layout','option'); ?>">			
					<?php include(locate_template('/modules/header-logo.php')); ?>
					<div class="header-content">
						<?php wp_nav_menu(array(
								'theme_location' => $nav_menu,
								'container'      => 'nav'
							));
						?>
						<?php 
							$headerImg = get_field('header_image','option');  
							$headerImgLink	= get_field('header_image_link','option');  
						if($headerImgLink) { ?>			
							<a href="<?php echo $headerImgLink; ?>" target="_blank" class="header-img" >
								<img src="<?php //echo $headerImg['url']; ?>"/>	
							</a>
						<?php } elseif($headerImg) { ?>
							<img class="header-img" src="<?php echo $headerImg['url']; ?>" alt="<?php echo $headerImg['alt']; ?>" />	
						<?php } 
						include(locate_template('/modules/social-media-links.php'));
						if($tel = get_field('telephone','option')) {
							echo '<a href="tel:' . $tel . ' " class="phone">';
							echo '<p>' . $tel . '</p>';
							echo '</a>';			
						}
						?>
						<?php include(locate_template('/modules/social-media-links.php')); ?>
						<div class="menu-toggle">
							<div class="one"></div>
							<div class="two"></div>
							<div class="three"></div>
						</div>
					</div>
				</div>
			</div>
		</header>
	<?php } ?>